//
// Created by Мари on 12/27/21.
//

#include <libc.h>
#include "../../include/tests.h"
#include "../../include/mem.h"
#include "../../include/mem_internals.h"
#include "../../include/util.h"

static void first_test() {
    void *first_malloc = _malloc(20);
    void *second_malloc = _malloc(50);
    void *third_malloc = _malloc(100);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    struct block_header *third_block = block_get_header(third_malloc);
    debug_heap(stdout, first_block);
    bool check_first = !first_block->is_free;
    bool check_second = !second_block->is_free;
    bool check_third = !third_block->is_free;
    if (check_first && check_second && check_third) {
        printf("First test passed\n");
        _free(first_malloc);
        _free(second_malloc);
        _free(third_malloc);
        debug_heap(stdout, first_block);
    } else err("First test failed\n");
}

static void second_test_1() {
    void *first_malloc = _malloc(20);
    void *second_malloc = _malloc(50);
    void *third_malloc = _malloc(100);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    struct block_header *third_block = block_get_header(third_malloc);
    debug_heap(stdout, first_block);
    _free(first_malloc);
    debug_heap(stdout, first_block);
    bool check_first = first_block->is_free;
    bool check_second = !second_block->is_free;
    bool check_third = !third_block->is_free;
    if (check_first && check_second && check_third) {
        printf("Second 1.0 passed\n");
        debug_heap(stdout, first_block);
        _free(second_malloc);
        debug_heap(stdout, first_block);
        _free(third_malloc);
        debug_heap(stdout, first_block);
    } else err("Second 1.0 test failed\n");
}

static void second_test_2() {
    void *first_malloc = _malloc(20);
    void *second_malloc = _malloc(50);
    void *third_malloc = _malloc(100);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    struct block_header *third_block = block_get_header(third_malloc);
    debug_heap(stdout, first_block);
    _free(second_malloc);
    debug_heap(stdout, first_block);
    bool check_first = !first_block->is_free;
    bool check_second = second_block->is_free;
    bool check_third = !third_block->is_free;
    if (check_first && check_second && check_third) {
        printf("Second 2.0 passed\n");
        debug_heap(stdout, first_block);
        _free(first_malloc);
        debug_heap(stdout, first_block);
        _free(third_malloc);
        debug_heap(stdout, first_block);
    } else err("Second 2.0 failed\n");
}

static void second_test_3() {
    void *first_malloc = _malloc(20);
    void *second_malloc = _malloc(50);
    void *third_malloc = _malloc(100);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    struct block_header *third_block = block_get_header(third_malloc);
    debug_heap(stdout, first_block);
    _free(third_malloc);
    debug_heap(stdout, first_block);
    bool check_first = !first_block->is_free;
    bool check_second = !second_block->is_free;
    bool check_third = third_block->is_free;
    if (check_first && check_second && check_third) {
        printf("Second 3.0 passed\n");
        _free(first_malloc);
        _free(second_malloc);
        debug_heap(stdout, first_block);
    } else err("Second 3.0 failed\n");
}
static void third_test() {
    void *first_malloc = _malloc(20);
    void *second_malloc = _malloc(50);
    void *third_malloc = _malloc(100);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    struct block_header *third_block = block_get_header(third_malloc);
    debug_heap(stdout, first_block);
    _free(second_malloc);
    _free(third_malloc);
    debug_heap(stdout, first_block);
    bool check_first = !first_block->is_free;
    bool check_second = second_block->is_free;
    bool check_third = third_block->is_free;
    if (check_first && check_second && check_third) {
        printf("Third passed\n");
        _free(first_malloc);
        debug_heap(stdout, first_block);
    } else err("Third failed\n");
}
void forth_test() {
    void *first_malloc = _malloc(10000);
    void *second_malloc = _malloc(5000);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    debug_heap(stdout, first_block);
    if (!first_block->is_free && !second_block->is_free) {
        printf("Forth passed\n");
        _free(first_malloc);
        _free(second_malloc);
        debug_heap(stdout, first_block);
    } else err("Forth failed\n");
}
void fifth_test() {
    void *first_malloc = _malloc(10000);
    void *second_malloc = _malloc(5000);
    struct block_header *first_block = block_get_header(first_malloc);
    struct block_header *second_block = block_get_header(second_malloc);
    mmap((void *) pages_count((size_t) second_block->next), 2000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void *allocated = _malloc(20000);

    if ((uint8_t *) second_block == (uint8_t *) allocated - offsetof(struct block_header, contents)) {
        err( "New region allocated in the old region! Fifth test failed");
    }
    debug_heap(stdout, first_block);
    _free(allocated);
    fprintf(stdout, "Fifth test passed");
}

void run_all_tests() {
    first_test();
    second_test_1();
    second_test_2();
    second_test_3();
    third_test();
    forth_test();
    fifth_test();
}