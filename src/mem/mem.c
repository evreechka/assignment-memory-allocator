#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "../../include/mem_internals.h"
#include "../../include/mem.h"
#include "../../include/util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

static int is_init_heap = false;
struct block_header *start_address;

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
//TODO
static struct region alloc_region(void const *addr, size_t query) {
    void *region_address = map_pages(addr, query, MAP_SHARED);
    while (region_address == MAP_FAILED) {
        region_address = map_pages(0, query, MAP_SHARED);
    }
    size_t actual_size = region_actual_size(query);
    block_init(region_address, (block_size) {actual_size}, NULL);
    return (struct region) {.addr= region_address, .size = actual_size, .extends = (addr == region_address)};
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//TODO
static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block_splittable(block, query)) {
        return false;
    }
    block_size second_block_size = {.bytes = block->capacity.bytes - query - offsetof(struct block_header, contents)};
    void *second_block_address = (void *) (block->contents + query);
    block_init(second_block_address, second_block_size, block->next);
    block->capacity.bytes = query;
    block->next = second_block_address;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

//TODO
static bool try_merge_with_next(struct block_header *block) {
    if (mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

//TODO
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    while (block) {
        if (block->is_free) {
            if (block_is_big_enough(sz, block)) {
                return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
            } else if (!try_merge_with_next(block)) {
                block = block->next;
            }
        } else {
            block = block->next;
        }
    }
    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = NULL};

}
//TODO
/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result block_find = find_good_or_last(block, query);
    if (block_find.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(block_find.block, query);
        block_find.block->is_free = false;
    }
    return block_find;

}


//TODO
static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    block_size size = (block_size) {region_actual_size(query + offsetof(struct block_header, contents))};
    if (map_pages(block_after(last), size.bytes, MAP_FIXED) != MAP_FAILED) {
        if (last->is_free) {
            last->capacity.bytes += size.bytes;
            return last;
        } else {
            block_init(block_after(last), size, NULL);
            last->next = block_after(last);
            return block_after(last);
        }
    }
    return NULL;

}
//TODO
/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result block_find = try_memalloc_existing(query, heap_start);
    if (block_find.type == BSR_FOUND_GOOD_BLOCK) return block_find.block;
    if (block_find.type == BSR_CORRUPTED) return NULL;
    struct block_header *new_block = grow_heap(block_find.block, query);
    if (new_block == NULL) return NULL;
    block_find = try_memalloc_existing(query, new_block);
    if (block_find.type == BSR_FOUND_GOOD_BLOCK) return block_find.block;
    return NULL;
}

void *_malloc(size_t query) {
    if (!is_init_heap) {
        start_address = heap_init((size_t) HEAP_START);
        is_init_heap = true;
    }
    if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
    struct block_header *const addr = memalloc(query, (struct block_header *) start_address);
    if (addr) return addr->contents;
    else return NULL;
}

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (header != NULL && header->next != NULL) {
        try_merge_with_next(header);
        header = header->next;
    }
}
